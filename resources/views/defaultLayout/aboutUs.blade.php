@extends('defaultLayout.defaultLayout')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{asset('css/MainPage.css')}}">

    <style>
        body{
            background-color: white;
            color: #333333;
        }
    </style>
@endsection

@section('content')
    @include('defaultLayout.mainPageNavBar')
    @include('defaultLayout.aboutUsContent')

@endsection


@section('footer')
    {{--<a href="{{route('privacy-info')}}" class="privacy">@lang('mainPage.privacy')</a>--}}
@endsection