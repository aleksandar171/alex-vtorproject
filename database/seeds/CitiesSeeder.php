<?php

use Illuminate\Database\Seeder;
use App\City;
class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = ['Aracinovo','Berovo','Bitola','Bogdanci','Bosilovo','Brvenica','Centar Zupa','Cesinovo-Oblesevo','Cucer-Sandevo','Delcevo','Demir Kapija','Dolneni','Gevgelija','Gradsko','Ilinden','Karbinci','Kicevo','Kocani','Konce','Kratovo','Kriva Palanka','Krivogastani','Krusevo','Lipkovo','Lozovo','Makedonska Kamenica','Negotino','Novo Selo','Pehcevo','Petrovec','Plasnica','Probistip','Radovis','Rankovce','Resen','Rosoman','Sopiste','Staro Nagoricane','Stip','Studenicani','Sveti Nikole','Tearce','Vasilevo','Vevcani','Vrapciste','Zelenikovo','Zelino','Zrnovci','Bogovinje','Caska','Debar','Demir Hisar','Gostivar','Jegunovce','Kavadarci','Kumanovo','Makedonski Brod','Mogila','Novaci','Ohrid','Prilep','Mavrovo i Rostusa','Dojran','Struga','Strumica','Tetovo','Valandovo','Veles','Vinica','Debarca','Skopje'];

        foreach ($cities as $city){
            $cityModel = new City();
            $cityModel->name = $city;
            $cityModel->save();
        }
    }
}
