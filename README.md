# Instructions

After cloning the project you need to run those commands to run the project

```php
    composer update
    cp .env.example .env
    php artisan key:generate
```

## After that you will need to change informations in .env file to match you local database, then run migrations and seeders

```php
    php artisan migrate
    php artisan db:seed
```

## Because project uses storage for storing images you will need to run this command to make link to public folder in laravel

```php
    php artisan storage:link
```


# Information about pdf download

For converting pdf project uses [Laravel Snappy PDF](https://github.com/barryvdh/laravel-snappy) and you will need [wkhtmltopdf](https://wkhtmltopdf.org/downloads.html) installed on
your linux machine for this to work locally . Also there is currently problem with the library, for some reason at random times can't convert blade file to pdf . 
We used server to test download, when you pass link that is hosted somewhere everything is working just fine . 

You can try converting blade file to html and than save than file somewhere and pass pure html to library . Bonus points for everyone that is going to try this . 
