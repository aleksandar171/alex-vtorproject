@extends('defaultLayout.defaultLayout')
	@section('head')
		<link rel="stylesheet" type="text/css" href="{{asset('css/MainPage.css')}}">
	@endsection

	@section('content')
		@include('defaultLayout.mainPageNavBar')
		@include('defaultLayout.MainPageContent')
		
	@endsection


@section('footer')

			<div class="col-md-12 mainPageFooter">
				<p class="pull-left" style="padding: 1em;padding-bottom: 0.5em">@lang('mainPage.footer')<a href="http://codepreneurs.co/" style="color: white; text-decoration: underline; font-weight: 700">Академијата за програмирање на Brainster</a></p>
				<a href="{{route('privacy-info')}}" class="pull-right" style="color: white;padding: 1em; padding-bottom: 0.5em; text-decoration: underline">@lang('mainPage.privacy')</a>
			</div>

	@endsection