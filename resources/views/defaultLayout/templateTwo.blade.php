
@extends('defaultLayout.defaultLayout')
    @section('head')
    <link rel="stylesheet" href="{{asset('css/profile/leftSection.css')}}" />
    <link rel="stylesheet" href="{{asset('css/profile/rightSection.css')}}" />
    <link rel="stylesheet" href="{{asset('css/profile/header.css')}}" />
    <meta name="theme-color" content="{{$user->color}}">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="no-cache">
    <meta http-equiv="Expires" content="-1">
    <meta http-equiv="Cache-Control" content="no-cache">
    @if(($public === false) && $user->id === Auth::id())
        <link rel="stylesheet" href="{{asset('css/profile/leftSectionAuth.css')}}" />
        {{--<link rel="stylesheet" href="{{asset('css/profile/rightSectionAuth.css')}}" />--}}
        {{--<link rel="stylesheet" href="{{asset('css/profile/headerAuth.css')}}" />--}}
    @endif

    <link rel="stylesheet" href="{{asset('EasyAutocomplete-1.3.5/easy-autocomplete.css')}}" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <script src="{{asset('EasyAutocomplete-1.3.5/jquery.easy-autocomplete.js')}}"></script>


    <script src="{{asset('js/jquery.tinycolorpicker.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/profile/leftSection.css')}}" />
    <script src="{{asset('js/jquery.tinycolorpicker.js')}}"></script>



@endsection

@section('navbar')
    @if($public === false)
    @include('navbar')
    @else
        <div class="d-flex-center">
            <h4>Креирано со</h4>
            <a style="margin-left: 1em; font-weight: 700" href="http://moecv.mk"> Moe CV</a>
        </div>
    @endif
@endsection

@section('content')


	<title>CV Template</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<link href="https://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet">

<!-- Add icon library -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" type="text/css" href="css/profile/style2.css">


</head>
<body>

<div class="container">
	<div class="row">

		<div class="profile-card-container col-md-4 col-md-offset-1 col-xs-12 text-center left d-flex-center flex-direction-column" >
			<div class="profile-image-container"
        @if($user->profile_photo !== null)
           style="background-image: url('{{asset('storage/'.$user->profile_photo)}}')"
       @endif>
           </div>
			
			<!-- <h2 class="text-uppercase"> {{ $user->first_name }} {{ $user->last_name }}</h2>
			<h4><em>{{$user->primary_title}}</em></h4>
			<ul class="list-unstyled">
				<li><span class="glyphicon glyphicon-map-marker"></span>{{$user->city->name}}</li>
				<li><span class="glyphicon glyphicon-envelope"> {{$user->email}}</li>
				<li><span class="glyphicon glyphicon-phone-alt"></span>   {{ $user->phone}}</li>
			</ul> -->
	<div style="margin-top: 2em">

           <h2 class="text-uppercase p-1">{{$user->first_name." ".$user->last_name}}</h2>
              <i  class="fa fa-pencil fa-2x pointer " id="editUserInfo" aria-hidden="true"></i>

          <h4>{{$user->job}}</h4>
           <div class="d-flex-center justify-flex-start p-1"><i class="fa fa-map-marker fa-2x" aria-hidden="true"></i><span>@if($user->city_id !==   null){{$user->city->name}}@endif</span></div>
           <div class="d-flex-center justify-flex-start p-1"><i class="fa fa-graduation-cap fa-2x" aria-hidden="true"></i><span>{{$user->degree}}</span></div>
            <div class="d-flex-center justify-flex-start p-1"><i class="fa fa-suitcase fa-2x" aria-hidden="true"></i><span>{{$user->job}}</span></div>
           <div class="d-flex-center justify-flex-start p-1"><i class="fa fa-envelope fa-2x" aria-hidden="true"></i><span>{{$user->email}}</span></div>
           <div class="d-flex-center justify-flex-start p-1"><i class="fa fa-phone fa-2x" aria-hidden="true"></i><span>{{$user->phone}}</span></div>
        {{--<div class="btn btn-default messageButton"><i class="fa fa-envelope" aria-hidden="true"></i>--}}
            {{--Message</div>--}}


   </div>
<!-- 
            <h2 class="blue">Looking for</h2>
            @if ($user->available_full_time == 1)

			<h4>Full time <span class="glyphicon glyphicon-ok"></span>
			</h4>
				@else
				<h4>Full time <span class="glyphicon glyphicon-remove"></span>
			</h4>
			@endif

            @if ($user->available_part_time == 1)
			<h4>Part time <span class="glyphicon glyphicon-ok"></span>
			</h4>
				@else
				<h4>Part time <span class="glyphicon glyphicon-remove"></span>
			</h4>
			@endif
			@if ($user->available_intern == 1)

			<h4>Freelance <span class="glyphicon glyphicon-ok"></span>
			</h4>
				@else
				<h4>Freelance <span class="glyphicon glyphicon-remove"></span>
			</h4>
			@endif -->
			{{--Icons--}}
    <h2 class="blue" style="background-color: {{$user->color}};padding-left: 70px;padding-right: 70px;">@lang('profile.available_text')</h2>

    <div class="profile-icons-container d-flex-center pointer">
        @if($user->available_full_time !== null)
            <div class="d-flex-center flex-direction-column m-1 icon-container">
                @if($user->available_full_time)
                    <a href="#"><i class="fa fa-check fa-2x" style="color: {{$user->color}}; margin-right: 0"></i></a>
                @else
                    <a href="#"><i class="fa fa-close fa-2x" style="color: {{$user->color}}; margin-right: 0"></i></a>
                @endif
                <p class="text-center">@lang('profile.profile_page_full_time')</p>

            </div>

            <div class="d-flex-center flex-direction-column m-1 icon-container">
                @if($user->available_part_time)
                    <a href="#"><i class="fa fa-check fa-2x" style="color: {{$user->color}}; margin-right: 0"></i></a>
                @else
                    <a href="#"><i class="fa fa-close fa-2x" style="color: {{$user->color}}; margin-right: 0"></i></a>
                @endif
                <p class="text-center">@lang('profile.profile_page_part_time')</p>
            </div>

            <div class="d-flex-center flex-direction-column m-1 icon-container">
                @if($user->available_intern)
                    <a href="#"><i class="fa fa-check fa-2x" style="color: {{$user->color}}; margin-right: 0"></i></a>
                @else
                    <a href="#"><i class="fa fa-close fa-2x" style="color: {{$user->color}}; margin-right: 0"></i></a>
                @endif
                <p class="text-center">@lang('profile.profile_page_freelance')</p>
            </div>
            {{--@if(($public === false) && $user->id === Auth::id())--}}
                {{--<div class="d-flex-center flex-direction-column m-1 edit-icon-container"--}}
                     {{--style="background-color: {{$user->color}}">--}}
                    {{--<p>Edit</p>--}}
                {{--</div>--}}
            {{--@endif--}}
        @else
            {{--@if(($public === false) && $user->id === Auth::id())--}}
                {{--<div class="alert alert-warning pointer availability_button" id="availability">Tell us about your--}}
                    {{--availability--}}
                {{--</div>--}}
            {{--@endif--}}
        @endif


    </div>

			<!-- <h2 class="blue">Top 3 Skills</h2>
			@foreach ($user->topThreeSkills as $skill)
			<h4>{{$skill->name}}</h4>
			@endforeach -->
		<div>
			<h2 class="blue" style="background-color: {{$user->color}};padding-left: 70px;padding-right: 70px;">@lang('profile.profile_page_top_three_skills')</h2>
            <h4>@foreach($user->topThreeSkills->reverse() as $skill)
               <p>{{$skill->name}}</p>
           @endforeach
           @if(($public === false) && $user->id === Auth::id())
               {{--<p class="pointer" id="addTopThreeSkills">+</p>--}}
           @endif</h4>
       </div>
			

			<!-- <h2 class="blue">Additional Skills</h2>
			<ul class="list-unstyled ">
				@foreach($user->skills->reverse() as $skill)
				<li class="skils">{{$skill->name}}</li>
				
			</ul>
			@endforeach -->
			<h2 class="text-center" style="background-color: {{$user->color}};padding-left: 70px;padding-right: 70px;">@lang('profile.profile_page_additional_skills')</h2>
        <div class="additional-skills">
        @foreach($user->skills->reverse() as $skill)
                <p style="background-color: {{$user->color}}; color: white">{{$skill->name}}</p>
            @endforeach
            @if(($public === false) && $user->id === Auth::id())
                {{--<p class="pointer" id="addAdditionalSkills" style="background-color: {{$user->color}}; color: white">--}}
                    {{--+</p>--}}
            @endif

        </div>

			<!-- <h2 class="blue"></h2>
			@foreach($user->languages as $language)
			<ul class="list-unstyled list-unstyled">
			
				<li>{{$language->name}} / {{$language->level}}</li>
				
			</ul>
			@endforeach -->
			{{--Languages--}}
    <div class="experienceContainer" style="width: 100%">
        <div class="d-flex-center experienceContainerIconContainer">
            {{--<img src="{{asset('images/joblogo.png')}}" class="img-responsive experienceContainerIcon" />--}}
            <div class="experienceContainerIcon d-flex-center" style="height: 5em; width: 5em; background-color: {{$user->color}}; border-radius: 50%; border: 5px solid white"><i class="fa fa-globe fa-2x text-white" style="margin-right: 0"></i></div>
        </div>
        <h4 class="text-center text-muted">@lang('profile.profile_page_language')</h4>
       @foreach($user->languages as $language)
        <h4>{{$language->name}}</h4>
        <p class="text-muted" style="padding-bottom: 2em">{{$language->level}}</p>
        @endforeach
    </div>
    {{--Social icons--}}
    <div class="social-icons-container d-flex-start flex-direction-column pointer"
         @if($user->id === Auth::id())id="socialMedia"@endif>
        @if(count($user->socialNetworks) > 0)

            @foreach($user->socialNetworks as $socialNetwork)
                @if(trim($socialNetwork->pivot->url) !== '' )
                    <a href="{{$socialNetwork->pivot->url}}"><i class="fa fa-{{$socialNetwork->name}}"
                  style="background-color: {{$user->color}}" aria-hidden="true"></i><span
                                style="margin-left: 2em; color: {{$user->color}}">{{$socialNetwork->pivot->url}}</span></a>
                @endif
            @endforeach
        @endif
    </div>
			

			<!-- <h2 class="blue">Social Media</h2>
			@foreach($user->socialNetworks as $socialNetwork)
		     <a href="{{$socialNetwork->pivot->url}}" class="fa fa-{{$socialNetwork->name}}"></a>
		     @endforeach -->
		   <!--   <a href="#" class="fa fa-twitter"></a>
		     <a href="#" class="fa fa-linkedin"></a>
		     <a href="#" class="fa fa-dribbble"></a> -->
		</div>


		<div class="col-md-6 col-xs-12 col-sm-12 text-center des" style="background-color: {{$user->color}}">
			<h2 class="bor"><em>"{{$user->profile_header_text}}"</em></h2>


			<h1 class="text-center">@lang('profile.profile_page_profile_profile')</h1>            
			@if($user->profile_header_text === null)
                   <p class="text-justify"></p>
           @else    
                   <div >
                  <p class="text-justify">{{$user->profile_header_text}}</p>
                  </div>           
                   @endif

   <!-- obrazovanie -->
         @foreach($educationExperience as $experience)  

         <div>    

			<h1 class="text-center">@lang('profile.profile_page_education')</h1>

			<h3 class="text-center">{{$experience->from}} - {{$experience->to}}</h3>
			 <h4 class="text-center m-top-bottom-2 experienceType">{{$experience->position}}  @lang('profile.profile_page_at') <a href="{{$experience->webpage}}" style="color: #333333; font-weight: 700; text-decoration: underline">{{$experience->company}}</a></h4>
			   <p  class="text-center m-top-bottom-2">{{$experience->info}}</p>
		 @endforeach 



		<!-- rabotno iskustvo -->

<h1 class="text-center">@lang('profile.profile_page_work_experience')</h1>
@foreach($currentWorkExperience as $experience)

	<h4 class="text-center ">{{$experience->from}} - present</h4>
        <h4 class="text-center m-top-bottom-2 experienceType">{{$experience->position}}  @lang('profile.profile_page_at') <a href="{{$experience->webpage}}" style="color: #333333; font-weight: 700;text-decoration: underline ">{{$experience->company}}</a> </h4>
        <p class="text-center m-top-bottom-2">{{$experience->info}}</p>
        @endforeach
@foreach($experiences as $experience)
			<h3 class="text-center ">{{$experience->from}} - {{$experience->to}}</h3>
			 <h4 class="text-center m-top-bottom-2 experienceType">{{$experience->position}}  @lang('profile.profile_page_at') <a href="{{$experience->webpage}}" style="color: #333333; font-weight: 700;text-decoration: underline">{{$experience->company}}</a></h4>
  <p class="text-center m-top-bottom-2">{{$experience->info}}</p>
			   @endforeach


<!-- proekti -->

	<h1 class="text-center">@lang('profile.profile_page_projects')</h1> 
	@foreach($projects as $experience)
	<div>

			  <h4 class="text-center ">{{$experience->from}} - {{$experience->to}}</h4>
        <h4 class="text-center m-top-bottom-2 experienceType"><a href="{{$experience->webpage}}" style="color: #333333; font-weight: 700; text-decoration: underline">{{$experience->position}}</a></h4>

        <p class="text-center m-top-bottom-2">{{$experience->info}}</p>
    </div>

       
        {{--@if(($public === false) && $user->id === Auth::id())--}}
    {{--<div class="addExperience alert alert-warning pointer alert-center" style="background-color: {{$user->color}}; color: white">Add experience</div>--}}
    {{--<div class="monster"></div>--}}

{{--@endif--}} 
			 @endforeach




<!-- @if(($public === false) && $user->id === Auth::id())
    @include('profile.modals.userInfoModal')
    @include('profile.modals.colorModal')
    @include('profile.modals.experienceModal')
    @include('profile.modals.shareProfileModal')
<script src="{{asset('js/profilePageRightSection.js')}}"></script>
 -->
@endif

@if(($public === false) && $user->id === Auth::id())
    @include('profile.modals.uploadPhotoModal')
    @include('profile.modals.availabilityModal')
    @include('profile.modals.topThreeSkillsModal')
    @include('profile.modals.additionalSkillsModal')
    @include('profile.modals.bestAdviceModal')
    {{--<script src="{{asset('js/dropzone.js')}}"></script>--}}
    <script src="{{asset('js/profilePage.js')}}"></script>
@endif

@if(($public === false) && $user->id === Auth::id())
   @include('profile.modals.uploadPhotoModal')
   {{--<script src="{{asset('js/dropzone.js')}}"></script>--}}
   <script src="{{asset('js/profilePage.js')}}"></script>
@endif
@endsection

@section('footer')
    @if($public === false)
    @include('profile.downloadpdf')
    @endif
    <script>
        var height = $( document ).height();
        var width = $( document ).width();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.post('/saveScreenDimensions',{height:height,width:width},function (data) {
            console.log(data);
        });
        



    </script>
    @if(($public === false) && $user->id === Auth::id())
   @include('profile.modals.colorModal')
   @include('profile.modals.shareProfileModal')
   <script src="{{asset('js/profilePageRightSection.js')}}"></script>
@endif
@endsection


