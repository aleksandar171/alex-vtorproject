<!DOCTYPE html>
<html lang="eng"
      xmlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#">
<head>

    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
    {{--<link href="https://fonts.googleapis.com/css?family=Nothing+You+Could+Do" rel="stylesheet">--}}
    {{--<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">--}}

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">
    <meta name="theme-color" content="#eb5d5c">
    <link rel="icon" sizes="192x192" href="{{asset('images/logo.png')}}">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->



    {{--Jquery--}}
    <script
            src="https://code.jquery.com/jquery-3.2.1.js"
            integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
            crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <link rel="stylesheet" href="{{asset('css/font-awesome-4.7.0/css/font-awesome.css')}}"/>

    <link rel="stylesheet" href="{{asset('EasyAutocomplete-1.3.5/easy-autocomplete.css')}}" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <script src="{{asset('EasyAutocomplete-1.3.5/jquery.easy-autocomplete.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/style.css')}}"/>
    <meta property="og:image" content="{{URL::asset('images/share.jpeg')}}"/>
    <meta property="og:image:type" content="image/jpeg">

@section('head')
    @show
</head>



<body>

@section('navbar')
@show


@section('content')
@show



@section('footer')

@show

</body>
</html>
